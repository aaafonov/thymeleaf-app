package ru.afonov.thymeleafapp.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.afonov.thymeleafapp.domain.TextEntry;
import ru.afonov.thymeleafapp.service.TextEntryService;

@Controller
@RequiredArgsConstructor
public class TextEntryController {

    private final TextEntryService textEntryService;

    @Value("${text.max.length}")
    private int maxLength;

    @GetMapping("/app")
    public String showForm(Model model) {
        model.addAttribute("textEntry", new TextEntry());
        model.addAttribute("entries", textEntryService.getAllEntries());
        model.addAttribute("maxLength", maxLength);
        return "index";
    }

    @PostMapping("/app")
    public String saveTextEntry(@Valid TextEntry textEntry, BindingResult bindingResult, Model model,
                                RedirectAttributes redirectAttributes) {
        if (textEntry.getText().length() > maxLength) {
            bindingResult.rejectValue("text", "text.too.long", "Текст слишком длинный");
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute("entries", textEntryService.getAllEntries());
            model.addAttribute("maxLength", maxLength);
            return "index";
        }

        textEntryService.saveEntry(textEntry);
        redirectAttributes.addFlashAttribute("successMessage", "Текст успешно сохранен");
        return "redirect:/app";
    }
}
