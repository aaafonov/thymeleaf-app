package ru.afonov.thymeleafapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.afonov.thymeleafapp.domain.TextEntry;

public interface TextEntryRepository extends JpaRepository<TextEntry, Long> {
}
