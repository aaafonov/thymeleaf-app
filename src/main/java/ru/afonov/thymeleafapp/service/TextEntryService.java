package ru.afonov.thymeleafapp.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.afonov.thymeleafapp.domain.TextEntry;
import ru.afonov.thymeleafapp.repository.TextEntryRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TextEntryService {

    private final TextEntryRepository textEntryRepository;

    public List<TextEntry> getAllEntries() {
        return textEntryRepository.findAll();
    }

    public TextEntry saveEntry(TextEntry textEntry) {
        return textEntryRepository.save(textEntry);
    }
}
